//set into list action
export const addProduct = (payload) => {
    return {
        type: 'ADD_PRODUCT',
        payload
    }
}

export function setDefault(payload) {
    return {
        type: 'SET_PRODUCT',
        payload
    }
};