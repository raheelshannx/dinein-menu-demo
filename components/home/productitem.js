import React from "react";
import {useDispatch, useSelector } from "react-redux";
import { setDefault } from "../../redux/actions/productAction";

function ProductItem(props) {

  const dispatch = useDispatch();
  const languages = useSelector((state) => state.language);
  const filtered = languages.filter((item) => item.isDefault);
  const lang = filtered[0];

  const handleClick = () => {
    dispatch(setDefault(props));
  };

  return (
    <div className="card">
      <a
        href="#"
        title="product detail"
        data-toggle="modal"
        data-target="#detailModal"
        onClick={handleClick}
      >
        <div className="img_block">
          <img
            src={props.image}
            className="w-100 product-item"
            alt={props[lang.short_name]["name"]}
            loading="lazy"
          />
        </div>
      </a>

      <div className="foodDetail ">
        <a
          href="#"
          title="product detail"
          data-toggle="modal"
          data-target="#detailModal"
          onClick={handleClick}
        >
          <h2>{props[lang.short_name]["name"]}</h2>
          <h3>{props.price}</h3>
        </a>
      </div>
    </div>
  );
}

export default ProductItem;
