import React from "react";
import Link from "next/link";
import { useSelector } from "react-redux";
import parse from "html-react-parser";
import ProductCardItem from "./productcardItem";

function ContentMenu(props) {
  const languages = useSelector((state) => state.language);
  const products = useSelector((state) => state.products);
  const filtered = languages.filter((item) => item.isDefault);
  const lang = filtered[0];
  const content =
    props.product[lang.short_name]["description"] != null
      ? props.product[lang.short_name]["description"]
      : "<p></p>";

  return (
    <div className="contentMenuWrapper">
      <div className="contentFood p-0">
        <div className="item_detail">
          <a className="back" href="#" title="index" data-dismiss="modal">
            <img src="assets/img/back.png" loading="lazy" />
          </a>
          <img
            loading="lazy"
            src={props.product.images[0]}
            className="w-100"
            alt={props.product[lang.short_name]["name"]}
          />
          <h2>{props.product[lang.short_name]["name"]}</h2>
          <h3>{props.product.price}</h3>
          <div>{parse(content)}</div>
        </div>
        {/* <ul className="nav nav-tabs" role="tablist">
          {products.map((product, index) => {
            if (props.product.category_id == product.category_id) {
              return (
                <ProductCardItem
                  key={product.product_id}
                  product={product}
                  id={props.product.product_id}
                  category_id={props.product.category_id}
                />
              );
            }
          })}
        </ul> */}
      </div>
    </div>
  );
}

export default ContentMenu;
